package puskapeli;

import java.io.*;

/**
 * Created by tomamon on 20.6.2016.
 */
public class CsvReader {

    private static CsvReader INSTANCE = null;

    private CsvReader(){}

    public static CsvReader getInstance(){
        if(INSTANCE == null){
            INSTANCE = new CsvReader();
        }
        return INSTANCE;
    }

    int rivilkm() throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream("images.csv"));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n')
                        ++count;
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if(endsWithoutNewLine) {
                ++count;
            }
            return count;
        } finally {
            is.close();
        }
    }

    public String[][] read(int koko) {

        String fileName = "images.csv";
        BufferedReader fileReader = null;
        String line;
        String csvLineSplitter = ",";
        String[][] kuvat = new String[koko][3];

        try {
            int i = 0;
            fileReader = new BufferedReader(new FileReader(fileName));
            while ((line = fileReader.readLine()) != null) {
                String[] split = line.split(csvLineSplitter);
                kuvat[i] = split;
                i++;
            }
        } catch (IOException e){
            e.printStackTrace();
        }finally {
            if (fileReader != null){
                try {
                    fileReader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Read images.csv successfully");
        return kuvat;

    }

}
