package puskapeli;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * Created by tomamon on 6.7.2016.
 */
public class View extends Application {

    private Controller ctrl = new Controller();
    private ProxyKuva[] kuvat;
    private Label lblPoints;
    private TextField txtLatin, txtDanish;
    private ImageView correctDk, correctLat, imageFrame;
    private Stage stage;

    private int runner = 0;
    private final Image CORRECT = new Image(getClass().getResourceAsStream("../correct.png"));
    private final Image INCORRECT = new Image(getClass().getResourceAsStream("../incorrect.png"));

    @Override
    public void init() {
        int i = 0;
        int koko = ctrl.rivilkm();
        String[][] rivit = ctrl.lue(koko);
        kuvat = new ProxyKuva[koko];
        System.out.println("Lines in file: " + koko);
        for (String[] tbl : rivit) {
            System.out.println("id: " + tbl[0] + ", lat: " + tbl[1] + ", dk: " + tbl[2] + ".");
            kuvat[i] = new ProxyKuva(Integer.parseInt(tbl[0]), tbl[1], tbl[2]);
            i++;
        }
        //ctrl.shuffle(kuvat);  // toimii, ota pois kun valmis
        System.out.println("Images loaded successfully");
        System.out.println(javafx.scene.text.Font.getFamilies());

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        stage = primaryStage;

        // top pane
        lblPoints = new Label("Your points: " + ctrl.getPointsTotal() + " / 200");

        MenuItem menuItemRestart = new MenuItem("_Restart Game");
        menuItemRestart.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN));
        menuItemRestart.setOnAction(e -> menuItemRestart_Clicked());
        Menu menuRestart = new Menu("_Menu");
        menuRestart.getItems().add(menuItemRestart);
        MenuBar menuBarRestart = new MenuBar();
        menuBarRestart.getMenus().add(menuRestart);

        // center pane
        imageFrame = new ImageView();
        imageFrame.setImage(kuvat[runner].getImage());
        imageFrame.prefWidth(640);
        imageFrame.prefHeight(360);
        imageFrame.setPreserveRatio(true);

        VBox paneCenter = new VBox();
        paneCenter.setAlignment(Pos.CENTER);
        lblPoints.setPadding(new Insets(30, 0, 30, 0));
        paneCenter.getChildren().addAll(lblPoints, imageFrame);

        // bottom pane
        // Danish pane
        Label lblDanish = new Label("Danish:");
        lblDanish.setPrefWidth(75);
        lblDanish.setAlignment(Pos.CENTER_LEFT);
        txtDanish = new TextField();
        txtDanish.setPrefColumnCount(37);
        txtDanish.setPromptText("Insert the name in Danish here.");
        txtDanish.setOnKeyReleased(e -> txtDanish_KeyReleased());

        correctDk = new ImageView();
        correctDk.setFitWidth(32);
        correctDk.setFitHeight(32);
        correctDk.setImage(INCORRECT);

        HBox paneDanish = new HBox(10, lblDanish, txtDanish, correctDk);
        paneDanish.setAlignment(Pos.CENTER);

        // Latin pane
        Label lblLatin = new Label("Latin:");
        lblLatin.setPrefWidth(75);
        lblLatin.setAlignment(Pos.CENTER_LEFT);
        txtLatin = new TextField();
        txtLatin.setPrefColumnCount(37);
        txtLatin.setPromptText("Insert the name in Latin here.");
        txtLatin.setOnKeyReleased(e -> txtLatin_KeyReleased());

        correctLat = new ImageView();
        correctLat.setFitWidth(32);
        correctLat.setFitHeight(32);
        correctLat.setImage(INCORRECT);

        HBox paneLatin = new HBox(10, lblLatin, txtLatin, correctLat);
        paneLatin.setAlignment(Pos.CENTER);

        Image imgLeftArrow = new Image(getClass().getResourceAsStream("../left.png"));
        Button btnPrevious = new Button();
        btnPrevious.setGraphic(new ImageView(imgLeftArrow));
        btnPrevious.setTooltip(new Tooltip("Previous picture"));
        btnPrevious.setOnAction(e -> btnPrevious_Click());

        Image imgRightArrow = new Image(getClass().getResourceAsStream("../right.png"));
        Button btnNext = new Button();
        btnNext.setDefaultButton(true);
        btnNext.setGraphic(new ImageView(imgRightArrow));
        btnNext.setTooltip(new Tooltip("Next picture"));
        btnNext.setOnAction(e -> btnNext_Click());

        Image imgRestart = new Image(getClass().getResourceAsStream("../restart.png"));
        Button btnReset = new Button();
        btnReset.setGraphic(new ImageView(imgRestart));
        btnReset.setTooltip(new Tooltip("Reset the game with the pictures you couldn't guess correctly!"));
        btnReset.setOnAction(e -> btnReset_Clicked());

        Region spacer1 = new Region();
        Region spacer2 = new Region();
        Region spacer3 = new Region();
        Region spacer4 = new Region();

        HBox paneNavButtons = new HBox(10, spacer1, spacer2, btnPrevious, btnNext, spacer3, btnReset, spacer4);

        HBox.setHgrow(spacer1, Priority.ALWAYS);
        HBox.setHgrow(spacer2, Priority.ALWAYS);
        HBox.setHgrow(spacer3, Priority.ALWAYS);
        HBox.setHgrow(spacer4, Priority.ALWAYS);

        VBox paneUserInput = new VBox(10, paneDanish, paneLatin, paneNavButtons);
        paneUserInput.setPadding(new Insets(20, 0, 20, 0));

        // finish the scene

        BorderPane pane = new BorderPane();
        pane.setTop(menuBarRestart);
        pane.setCenter(paneCenter);
        pane.setBottom(paneUserInput);

        Scene scene = new Scene(pane);
        primaryStage.setMinWidth(802);
        primaryStage.setMinHeight(888);
        primaryStage.setMaxWidth(1880);
        primaryStage.setMaxHeight(1000);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Bushwhacker 2016!");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("../tree.png")));
        primaryStage.show();

    }

    private void btnReset_Clicked() {

        ProxyKuva[] array = new ProxyKuva[ctrl.rivilkm()];

        int j = 0;
        for (int i = 0; i < kuvat.length; i++){
            if (!kuvat[i].getCorrectDk() | !kuvat[i].getCorrectLat()){
                array[j] = kuvat[i];
                j++;
            }
        }

        int nulls = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null){
                nulls++;
            }
        }

        kuvat = new ProxyKuva[array.length-nulls];

        for (int i = 0; i < kuvat.length; i++){
            kuvat[i] = new ProxyKuva(array[i]);
        }

        ctrl.shuffle(kuvat);

        runner = 0;
        imageFrame.setImage(kuvat[runner].getImage());
        ctrl.resetPoints();
        lblPoints.setText("Your points: " + ctrl.getPointsTotal() + " / " + kuvat.length * 2);

    }

    private void menuItemRestart_Clicked() {

        init();

        ctrl.shuffle(kuvat);
        ctrl.resetPoints();

        for (ProxyKuva pk : kuvat){
            pk.setCorrectDk(false);
            pk.setCorrectLat(false);
        }

        lblPoints.setText("Your points: " + ctrl.getPointsTotal() + " / " + kuvat.length * 2);

        correctLat.setImage(INCORRECT);
        correctDk.setImage(INCORRECT);

        txtDanish.clear();
        txtLatin.clear();
        txtLatin.setEditable(true);
        txtDanish.setEditable(true);
        txtDanish.requestFocus();

        runner = 0;
        imageFrame.setImage(kuvat[runner].getImage());

    }

    private void txtDanish_KeyReleased() {
        if (txtDanish.getText().equals(kuvat[runner].getNameDanish())){
            correctDk.setImage(CORRECT);
            if (!kuvat[runner].getCorrectDk()){
                ctrl.setPointsDanish(ctrl.getPointsDanish()+1);
                txtDanish.setEditable(false);
            }
            kuvat[runner].setCorrectDk(true);
        }
        lblPoints.setText("Your points: " + ctrl.getPointsTotal() + " / " + kuvat.length * 2);
    }

    private void txtLatin_KeyReleased() {
        if (txtLatin.getText().equals(kuvat[runner].getNameLatin())){
            correctLat.setImage(CORRECT);
            if (!kuvat[runner].getCorrectLat()){
                ctrl.setPointsLatin(ctrl.getPointsLatin()+1);
                txtLatin.setEditable(false);
            }
            kuvat[runner].setCorrectLat(true);
        }
        lblPoints.setText("Your points: " + ctrl.getPointsTotal() + " / " + kuvat.length * 2);
    }

    private void btnNext_Click() {

        /*
        jos käyttäjä haluaa palata edelliseen kuvaan, oikeat vastaukset näkyvät, focus on txtdanish, iconit oikein
        paljon toistoa näissä kahdessa, refactorointia.
         */

        if (runner == kuvat.length-1) {
            runner = 0;
        }else{
            runner++;
        }

        boolean boolLat = kuvat[runner].getCorrectLat();
        boolean boolDk = kuvat[runner].getCorrectDk();

        txtLatin.clear();
        txtDanish.clear();

        if (!boolDk){
            correctDk.setImage(INCORRECT);
            txtDanish.setEditable(true);
        }else{
            correctDk.setImage(CORRECT);
            txtDanish.setText(kuvat[runner].getNameDanish());
            txtDanish.setEditable(false);
        }
        if (!boolLat){
            correctLat.setImage(INCORRECT);
            txtLatin.setEditable(true);
        }else{
            correctLat.setImage(CORRECT);
            txtLatin.setText(kuvat[runner].getNameLatin());
            txtLatin.setEditable(false);
        }

        imageFrame.setImage(kuvat[runner].getImage());

        if (!boolDk){
            txtDanish.requestFocus();
        }

        if (boolDk && !boolLat){
            txtLatin.requestFocus();
        }

        System.out.println("next " + runner);
    }

    private void btnPrevious_Click() {

        if (runner == 0){
            runner = kuvat.length-1;
        }else{
            runner--;
        }

        boolean boolLat = kuvat[runner].getCorrectLat();
        boolean boolDk = kuvat[runner].getCorrectDk();

        txtLatin.clear();
        txtDanish.clear();

        if (!boolDk){
            correctDk.setImage(INCORRECT);
            txtDanish.setEditable(true);
        }else{
            correctDk.setImage(CORRECT);
            txtDanish.setText(kuvat[runner].getNameDanish());
            txtDanish.setEditable(false);
        }
        if (!boolLat){
            correctLat.setImage(INCORRECT);
            txtLatin.setEditable(true);
        }else{
            correctLat.setImage(CORRECT);
            txtLatin.setText(kuvat[runner].getNameLatin());
            txtLatin.setEditable(false);
        }

        imageFrame.setImage(kuvat[runner].getImage());

        if (!boolDk){
            txtDanish.requestFocus();
        }

        if (boolDk && !boolLat){
            txtLatin.requestFocus();
        }

        System.out.println("prev " + runner);

    }

    public static void main(String[] args) {
        launch(args);
    }

}
