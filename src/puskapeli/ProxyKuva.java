package puskapeli;

import javafx.scene.image.Image;

/**
 * Created by tomamon on 21.6.2016.
 */
public class ProxyKuva implements Kuva {

    private RealKuva kuva = null;
    private String nameInDanish, nameInLatin;
    private int fileNumber;
    private Boolean correctDk = false;
    private Boolean correctLat = false;

    /**
     * Constructor
     * @param fileNumber FileNumber as presented in /pictures/
     * @param nameInDanish Danish name as presented in images.csv
     * @param nameInLatin Latin name as presented in images.csv
     */
    public ProxyKuva(final int fileNumber, final String nameInLatin, final String nameInDanish){
        this.fileNumber = fileNumber;
        this.nameInDanish = nameInDanish;
        this.nameInLatin = nameInLatin;
    }

    public ProxyKuva(ProxyKuva p){
        System.out.println("Kopiokonstruktori:");
        System.out.println(p.getInfo());
        if (p.getKuva() == null){
            this.kuva = null;
        }else{
            this.kuva = p.getKuva();
        }
        this.nameInDanish = p.getNameDanish();
        this.nameInLatin = p.getNameLatin();
        this.fileNumber = p.getFileNumber();
        this.correctDk = p.getCorrectDk();
        this.correctLat = p.getCorrectLat();
    }

    /**
     * Creates a RealKuva object
     * @return {@link RealKuva} file
     */
    @Override
    public Image getImage() {
        if(kuva == null){
            kuva = new RealKuva(this.fileNumber, this.nameInDanish, this.nameInLatin);
        }
        return kuva.getImage();
    }

    /**
     * Get method for variable kuva
     * @return kuva
     */
    public RealKuva getKuva() {
        return this.kuva;
    }

    /**
     *
     * @return Filenumber as presented in pictures/
     */
    @Override
    public int getFileNumber() {
        return this.fileNumber;
    }

    /**
     * @return Latin name as presented in images.csv
     */
    @Override
    public String getNameLatin() {
        return this.nameInLatin;
    }

    /**
     *
     * @return Danish name as presented in images.csv
     */
    @Override
    public String getNameDanish() {
        return this.nameInDanish;
    }

    /**
     * 
     * @return Information about the picture
     */
    @Override
    public String getInfo() {return "id: " + fileNumber + ", lt: " + nameInLatin + ", dk: " + nameInDanish + ".";}

    @Override
    public Boolean getCorrectLat() {
        return this.correctLat;
    }

    @Override
    public Boolean getCorrectDk() {
        return this.correctDk;
    }

    @Override
    public void setCorrectLat(Boolean b) {
        this.correctLat = b;
    }

    @Override
    public void setCorrectDk(Boolean b) {
        this.correctDk = b;
    }

}
