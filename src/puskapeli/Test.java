package puskapeli;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by tomamon on 21.6.2016.
 */
public class Test extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        CsvReader lukija = CsvReader.getInstance();
        String[][] csv = new String[4][3];
        ArrayList<ProxyKuva> kuvat = new ArrayList<>();

        csv = lukija.read(0);

        for (String[] tbl : csv){
            System.out.println("id: " + tbl[0] + ", lat: " + tbl[1] + ", dk: " + tbl[2] + ".");
            ProxyKuva kuva = new ProxyKuva(Integer.parseInt(tbl[0]), tbl[1], tbl[2]);
            kuvat.add(kuva);
        }

        for (ProxyKuva k : kuvat) {
            System.out.println(k.getInfo());
        }

    }
}
