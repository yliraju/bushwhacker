package puskapeli;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by tomamon on 9.7.2016.
 */
public class Model {

    private int pointsDanish, pointsLatin = 0;

    public void shuffle(ProxyKuva[] pictures){

        Random rnd = ThreadLocalRandom.current();
        for (int i = pictures.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            ProxyKuva a = pictures[index];
            pictures[index] = pictures[i];
            pictures[i] = a;
        }

    }

    public int getPointsDanish() {
        return pointsDanish;
    }

    public void setPointsDanish(int pointsDanish) {
        this.pointsDanish = pointsDanish;
    }

    public int getPointsLatin() {
        return pointsLatin;
    }

    public void setPointsLatin(int pointsLatin) {
        this.pointsLatin = pointsLatin;
    }

    public int getPointsTotal() {
        return this.pointsDanish + this.pointsLatin;
    }

    public void resetPoints() {
        this.pointsDanish = 0;
        this.pointsLatin = 0;
    }

}
