package puskapeli;

import javafx.scene.image.Image;
import java.io.File;

/**
 * Created by tomamon on 21.6.2016.
 */
class RealKuva implements Kuva {

    private final int fileNumber;
    private final String nameLatin;
    private final String nameDanish;
    private Image picture;
    private Boolean correctLat, correctDk = false;

    /**
     * Constructor
     * @param fileNumber FileNumber as presented in /pictures/
     * @param nameInDanish Danish name as presented in images.csv
     * @param nameInLatin Latin name as presented in images.csv
     */
    public RealKuva(int fileNumber, String nameInLatin, String nameInDanish) {
        this.fileNumber = fileNumber;
        this.nameDanish = nameInDanish;
        this.nameLatin = nameInLatin;
        File pictureSource = new File("pictures/" + fileNumber + ".jpg");
        this.picture = new Image(pictureSource.toURI().toString());
    }

    /**
     *
     * @return Filenumber as presented in pictures/
     */
    public int getFileNumber()
    {
        return this.fileNumber;
    }

    /**
     *
     * @return Latin name as presented in images.csv
     */
    public String getNameLatin()
    {
        return this.nameLatin;
    }

    /**
     *
     * @return Danish name as presented in images.csv
     */
    public String getNameDanish()
    {
        return this.nameDanish;
    }

    /**
     *
     * @return {@link Image} file
     */
    @Override
    public Image getImage()
    {
        return this.picture;
    }

    /**
     *
     * @return Information about the picture
     */
    @Override
    public String getInfo()
    {
        return "id: " + fileNumber + ", lt: " + nameLatin + ", dk: " + nameDanish + ".";
    }

    @Override
    public Boolean getCorrectLat() {
        return this.correctLat;
    }

    @Override
    public Boolean getCorrectDk() {
        return this.correctDk;
    }

    @Override
    public void setCorrectLat(Boolean b) {
        this.correctLat = b;
    }

    @Override
    public void setCorrectDk(Boolean b) {
        this.correctDk = b;
    }
}
