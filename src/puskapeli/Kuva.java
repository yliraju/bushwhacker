package puskapeli;

import javafx.scene.image.Image;

/**
 * Created by tomamon on 21.6.2016.
 */
public interface Kuva {

    Image getImage();
    int getFileNumber();
    String getNameLatin();
    String getNameDanish();
    String getInfo();
    Boolean getCorrectLat();
    Boolean getCorrectDk();
    void setCorrectLat(Boolean b);
    void setCorrectDk(Boolean b);

}
