package puskapeli;

import java.io.IOException;

/**
 * Created by tomamon on 21.6.2016.
 */
public class Controller {

    private CsvReader lukija = CsvReader.getInstance();
    private Model model = new Model();

    /**
     * Reads the number of lines on images.csv
     * @return The number of lines
     */
    public int rivilkm() {
        int i = 0;
        try {
            i = lukija.rivilkm();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return i;
    }

    public int getPointsDanish() {
        return model.getPointsDanish();
    }

    public void setPointsDanish(int pointsDanish) {
        model.setPointsDanish(pointsDanish);
    }

    public int getPointsLatin() {
        return model.getPointsLatin();
    }

    public void setPointsLatin(int pointsLatin) {
        model.setPointsLatin(pointsLatin);
    }

    public int getPointsTotal() {
        return model.getPointsTotal();
    }

    public void shuffle(ProxyKuva[] kuvat) {
        model.shuffle(kuvat);
    }

    /**
     * Method reads the images.csv file
     * @param i The number of lines in the images.csv file. Used to create a correct sized table.
     * @return Two dimensional array including lines read from images.csv
     */
    public String[][] lue(int i) {
        return lukija.read(i);
    }


    public void resetPoints() {
        model.resetPoints();
    }
}
